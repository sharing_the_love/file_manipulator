if ARGV[0].nil?
  puts "please provide a target dir like '/media/me/todos/pics/**/*.jpg'"
  return
end

current_files = Dir.entries('.').reject { |f| File.directory? f }
target_files = Dir[ARGV[0]].map { |f| File.basename(f) if !File.directory? f }
common_files = current_files & target_files

puts current_files - common_files

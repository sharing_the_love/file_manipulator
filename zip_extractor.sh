#!/bin/bash

if ! command -v unzip &> /dev/null
then
    echo "unzip could not be found. Please intall it and try again."
    exit
fi

for filename in *.zip ; do
	unzip $filename
done
